import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { PureTaskList } from "./PureTaskList";
import { archiveTask, pinTask } from "../../store/redux";

export const TaskList = () => {
  const dispatch = useDispatch();
  const tasks = useSelector((s) => s.tasks);
  const onArchiveTask = (id) => dispatch(archiveTask(id));
  const onPinTask = (id) => dispatch(pinTask(id));

  const props = { tasks, onArchiveTask, onPinTask };

  return <PureTaskList {...props}></PureTaskList>;
};
